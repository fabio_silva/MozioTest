String.prototype.repeatify = function(times) {
    var returnValue = "";
    if (!times || times < 0) times = 1;
    var count = 0;
    
    while (count < times) {
        returnValue += String(this);
        count++;
    }
    
    return returnValue;
};

//console.log('Mozio'.repeatify(3));

function removeDuplicates(arr) {
    var arrReturn = [];
    
    arr.forEach(function(item) {
        if (arrReturn.indexOf(item) === -1) arrReturn.push(item);
    }); 
    
    return arrReturn;
}

// console.log(removeDuplicates(['a', 'b', 'a', 'c', 'b']));

function validateEmail(str) {
    var regexp = /[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*/;
    return regexp.test(str);
}

$(function() {
    var form = $("form");
    var btnSubmit = $("#btnSubmit");
    var duration = 300;
    
    hideAlerts = function() {
        $("#divRequired").hide(duration);
        $("#divInvalidEmail").hide(duration);
        $("#divSuccess").hide(duration);   
        $("#divError").hide(duration);   
    };
    
    initForm = function() {    
        $.each(form.serializeArray(), function(i, field) {
            var fieldObj = $("#" + field.name);
            fieldObj.val("");
            fieldObj.parent().removeClass("has-error");
        });
        
        hideAlerts();
        
        btnSubmit.prop("disabled", false);
        
        $("#firstName").focus();
    };    
    
    form.submit(function(event) {
        hideAlerts();
        event.preventDefault();
        
        btnSubmit.prop("disabled", true);

        var isValid = true;
        var fieldFocus;
        $.each(form.serializeArray(), function(i, field) {
            var fieldObj = $("#" + field.name);
            var fieldVal = fieldObj.val();
            var fieldIsValid = true;

            fieldObj.parent().removeClass("has-error");
            
            if (fieldObj.prop('required') && fieldVal === "") {                
                fieldIsValid = false;
            }
            if (fieldObj.prop('type') === "email" && !validateEmail(fieldVal)) {
                $("#divInvalidEmail").show(duration);
                fieldIsValid = false;
            }
            
            if (!fieldIsValid) isValid = false;
            
            if (fieldIsValid) {
                fieldObj.parent().removeClass("has-error");
            } else {
                if (!fieldFocus) fieldFocus = fieldObj;
                fieldObj.parent().addClass("has-error");
            }
        });   
        
        if (fieldFocus) fieldFocus.focus();
        
        if (isValid) {
            var data = {};
            $.each(form.serializeArray(), function(i, field) {
                data[field.name] = field.value;
            });
            $.ajax({
              method: "POST",
              url: 'https://httpbin.org/post',
              data: data
            })
            .done(function(msg) {
                $("#divSuccess").show(duration);
                setTimeout(function() { 
                    initForm();
                }, 3000);
            })
            .fail(function(jqXHR, textStatus) {
                $("#divError").show(duration);
            });
        } else {
            btnSubmit.prop("disabled", false);
            $("#divRequired").show(duration);
        }
    });
    
    initForm();
});